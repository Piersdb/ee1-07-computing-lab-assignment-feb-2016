// ProgrammingAssignment.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <fstream>

using namespace std;


//namespace ProgrammingAssignment {

void WriteArrayToConsole(int input[16]);
bool ShiftUp(int input[16]);
int Shift(int* input[4]);
void InitializeToZero(int input[16]);
void AssignRandomSquare(int input[16]);
bool ShiftLeft(int input[16]);
bool ShiftRight(int input[16]);
bool ShiftDown(int input[16]);
bool ArrayIsFull(int input[16]);


int main()
{
	cout << "Enter Input filename" << endl;
	//File Input
	string fileName;
	cin >> fileName;

	int* inputArray = new int[16];
	fstream infile;

	infile.open(fileName);



	if (infile.is_open()) {
		cout << "Oppening File" << endl;
		for (int i = 0; i < 16; i++) {
			infile >> inputArray[i];
		}

	}
	//Generate Default A
	else {
		cout << "The File Was not found. Creating new Grid;" << endl;
		
		InitializeToZero(inputArray);
		inputArray[15] = 2;
	}
	inputArray[0] = 2;
	inputArray[1] = 4;
	inputArray[2] = 2;
	inputArray[3] = 0;
	

	WriteArrayToConsole(inputArray);
	
	bool GameIsRunning = true;
	string InputCharacter;
	
	bool MoveExecuted = false;
	while (GameIsRunning)
	{
		MoveExecuted = false;
		getline(cin, InputCharacter);
		switch (InputCharacter[0]) {
		case 'w':
			if (ShiftUp(inputArray)) {
				MoveExecuted = true;
			}
			break;
		case 'a':
			if (ShiftLeft(inputArray)) {
				MoveExecuted = true;
			}
			break;
		case 's':
			if (ShiftDown(inputArray)) {
				MoveExecuted = true;
			}
			break;
		case 'd':
			if (ShiftRight(inputArray)) {
				MoveExecuted = true;
			}
			break;
		default:
			cout << "Input not recognised" << endl;
			break;
		}

		if (MoveExecuted == true) {
			AssignRandomSquare(inputArray);
			WriteArrayToConsole(inputArray);
		}
		else {
			//WriteArrayToConsole(inputArray);
			//cout << "Move Not Valid" << endl;
		}


		if (ArrayIsFull(inputArray)) {

			int* FinalTestArray = new int[16];

			for (int i = 0; i < 16; i++) {
				FinalTestArray[i] = inputArray[i];
			}


			if (ShiftUp(FinalTestArray) == true) {
				GameIsRunning = false;
			}
			else if (ShiftDown(FinalTestArray) == true) {
				GameIsRunning = false;
			}
			else if (ShiftLeft(FinalTestArray) == true) {
				GameIsRunning = false;
			}
			else if (ShiftRight(FinalTestArray) == true) {
				GameIsRunning = false;
			}
		}


	}


	WriteArrayToConsole(inputArray);


	delete[] inputArray;
	system("pause");
    return 0;
}

void WriteArrayToConsole(int input[16] ) {
	for (int i = 0; i < 16; i++) {
		if (i% 4 == 0) {
			cout << endl;
		}
		cout << input[i] << "\t";
	}
	cout << endl << endl;
}


bool ArrayIsFull(int input[16]) {

	for (int i = 0; i < 16; i++) {
		if (input[i] == 0) {
			return false;
		}
	}
	return true;

}

void InitializeToZero(int input[16]){
	memset(input, 0, sizeof(int)*16);


}

void AssignRandomSquare(int input[16]) {
	while (true) {
		int randomCell = rand() % 16;
		if (input[randomCell] == 0) {
			input[randomCell] = 2;
			return;
		}
	}
}


bool ShiftLeft(int input[16]) {
	int shiftsMade = 0;
	int* ShiftArray[4];
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			ShiftArray[j] = &input[(i * 4)+j];
		}
		shiftsMade+= Shift(ShiftArray);

	}
	if (shiftsMade > 0) {
		return true;
	}
	return false;

}

bool ShiftUp(int input[16]) {
	int shiftsMade = 0;
	int* ShiftArray[4];
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			ShiftArray[j] = &input[(j*4)+i];
		}
		shiftsMade += Shift(ShiftArray);

	}
	if (shiftsMade > 0) {
		return true;
	}
	return false;
}

bool ShiftRight(int input[16]) {
	int shiftsMade = 0;
	int* ShiftArray[4];
	for (int i = 0; i <4; i++) {
		for (int j = 3; j >=0; j--) {
			ShiftArray[(3-j)] = &(input[((i * 4) + j)]);
		}
				shiftsMade += Shift(ShiftArray);

	}
	if (shiftsMade > 0) {
		return true;
	}
	return false;


}

bool ShiftDown(int input[16]) {
	int shiftsMade = 0;
	int* ShiftArray[4];
	for (int i = 0; i < 4; i++) {
		for (int j = 3; j >=0; j--) {
			ShiftArray[3-j] = &input[(j * 4) + i];
		}
		shiftsMade += Shift(ShiftArray);

	}
	if (shiftsMade > 0) {
		return true;
	}
	return false;

}



int Shift(int* input[4]) {
	int ShiftsMade = 0;

	for (int i = 0; i <4; i++) {
		if (*input[i] == 0) {
			for (int j = (i+1); j < 4; j++) {
				if (*input[j]!=0&&*input[i]==0) {
					*input[i] = *input[j];
					*input[j] = 0;
					ShiftsMade += 1;
				}
				else if (*input[j]!=0 && *input[i] != 0) {
					if (*input[i] == *input[j]) {
						*input[i] += *input[j];
						*input[j] = 0;
						ShiftsMade += 1;
					}
						j = 100;



				}


			}
		}

		else {
			for (int j = (i + 1); j < 4; j++) {

					if (*input[i] == *input[j]) {
						*input[i] += *input[j];
						*input[j] = 0;
						ShiftsMade += 1;
						j = 100;
					}
					else if (*input[j] != 0) {
						j = 100;
					}


			}
		
		
		}

	}


		return ShiftsMade;



}
//}